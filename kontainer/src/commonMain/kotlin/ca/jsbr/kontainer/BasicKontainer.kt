@file:Suppress("unused", "UNCHECKED_CAST")

package ca.jsbr.kontainer

import ca.jsbr.kontainer.factory.Factory

class BasicKontainer(val map: HashMap<Any, Factory<*>> = HashMap()) : Kontainer {
    override fun <T : Any, T2 : Factory<T>> set(key: Any, factory: T2): T2 {
        map[key] = factory
        return factory
    }

    override fun <T : Any> get(key: Any): Factory<T>? = map[key] as Factory<T>?
    override fun contains(key: Any): Boolean = key in map
}

