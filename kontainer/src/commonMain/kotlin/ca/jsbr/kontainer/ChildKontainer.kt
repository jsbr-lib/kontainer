@file:Suppress("unused", "UNCHECKED_CAST")

package ca.jsbr.kontainer

import ca.jsbr.kontainer.factory.Factory
import ca.jsbr.kontainer.factory.value

class ChildKontainer(val child: Kontainer, var parent: Kontainer) : Kontainer by child {

    init {
        set(Kontainer::class, value(this))
    }

    override fun <T : Any> get(key: Any): Factory<T>? = child[key]
            ?: parent[key]

    override fun contains(key: Any): Boolean = child.contains(key) || parent.contains(key)
}


