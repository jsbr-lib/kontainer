package ca.jsbr.kontainer

import ca.jsbr.kontainer.factory.Factory

interface Kontainer {
    operator fun <T : Any, T2: Factory<T>> set(key: Any, factory: T2): T2
    operator fun <T : Any> get(key: Any): Factory<T>?
    operator fun contains(key: Any): Boolean
}
