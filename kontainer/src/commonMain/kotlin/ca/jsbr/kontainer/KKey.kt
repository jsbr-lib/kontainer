@file:Suppress("unused")

package ca.jsbr.kontainer

class KKey<T>(private val debug: String? = null) {
    override fun toString(): String {
        return debug ?: super.toString()
    }
}

fun <T> keyOf(debug: String? = null) = KKey<T>(debug)
