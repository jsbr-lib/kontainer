package ca.jsbr.kontainer.exception

class KontainerError(message: String, cause: Throwable? = null) : Exception(message, cause)


fun NOT_FOUND(key: String): Nothing = throw KontainerError("Unable to find factory for: $key")
fun UNALBE_TO_RESOLVE(key: String, cause: Throwable): Nothing =
    if (cause is KontainerError) throw cause else throw KontainerError("Unable to resolve: $key", cause)

fun UNALBE_TO_CHANGE_SINGE_LIST(): Nothing = throw KontainerError("UCan't not change single list after creation")