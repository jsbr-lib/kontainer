@file:Suppress("unused")

package ca.jsbr.kontainer

import ca.jsbr.kontainer.exception.NOT_FOUND
import ca.jsbr.kontainer.exception.UNALBE_TO_RESOLVE

/**
 * instance: require, get, i, di
 */


fun kontainer(init: Kontainer.() -> Unit): Kontainer = BasicKontainer().apply(init)

fun <T : Any> Kontainer.instance(kKey: KKey<T>): T {
    try {
        return get<T>(kKey)?.create(this) ?: NOT_FOUND(kKey.toString())
    } catch (err: Throwable) {
        UNALBE_TO_RESOLVE(kKey.toString(), err)
    }
}

fun <T : Any> Kontainer.require(Key: Any): T {
    try {
        return get<T>(Key)?.create(this) ?: NOT_FOUND(Key.toString())
    } catch (err: Throwable) {
        UNALBE_TO_RESOLVE(Key.toString(), err)
    }
}


fun <T : Any> Kontainer.instance(Key: Any): T {
    try {
        return get<T>(Key)?.create(this) ?: NOT_FOUND(Key.toString())
    } catch (err: Throwable) {
        UNALBE_TO_RESOLVE(Key.toString(), err)
    }
}


inline fun <reified T : Any> Kontainer.instance(): T {
    try {
        return get<T>(T::class)?.create(this) ?: NOT_FOUND(T::class.simpleName!!)
    } catch (err: Throwable) {
        UNALBE_TO_RESOLVE(T::class.simpleName!!, err)
    }
}

inline fun <reified T : Any> Kontainer.resolve(): T? =
    get<T>(T::class)?.create(this)


fun <T : Any> Kontainer.instance(any: Any, block: Kontainer.() -> Unit): T =
    ChildKontainer(kontainer(block), this).get<T>(any)?.create(this) ?: NOT_FOUND(any.toString())


fun Kontainer.extension(block: Kontainer.() -> Unit): Kontainer =
    ChildKontainer(BasicKontainer(), this).apply(block)
