package ca.jsbr.kontainer.factory

import ca.jsbr.kontainer.Kontainer

interface Factory<out T> {
    fun create(kontainer: Kontainer): T
}