package ca.jsbr.kontainer.factory

import ca.jsbr.kontainer.*
import ca.jsbr.kontainer.Kontainer

/**
 * single is persistent
 */
class SingleChildGroupFactory<T : Any>(val selfKtn: Kontainer, val factory: Factory<T>) : Factory<T> {
    val childKtn: ChildKontainer = ChildKontainer(selfKtn, selfKtn)
    override fun create(kontainer: Kontainer): T {
        childKtn.parent = kontainer
        return factory.create(childKtn)
    }
}

/**
 * Create a persistent child container and expose only the last (returned) factory to the parent
 * NB: parent factory is available for child container
 */
inline fun <reified T : Any> Kontainer.child(key: Any = T::class, init: Kontainer.() -> Factory<T>): Factory<T> {
    val k = BasicKontainer()
    val factory = SingleChildGroupFactory(k, k.let(init))
    return this.set(key, factory)
}

/**
 * Create a persistent child container and expose only the last (returned) factory to the parent
 * NB: parent factory is available for child container
 */
fun <T : Any> Kontainer.child(key: KKey<T>, init: Kontainer.() -> Factory<T>): SingleChildGroupFactory<T> {
    val k = BasicKontainer()
    val factory = SingleChildGroupFactory(k, k.let(init))
    return this.set(key, factory)
}
