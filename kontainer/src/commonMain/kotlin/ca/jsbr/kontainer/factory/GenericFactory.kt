package ca.jsbr.kontainer.factory

import ca.jsbr.kontainer.BasicKontainer
import ca.jsbr.kontainer.Kontainer

class GenericFactory<T : Any>(private val listKontainer: Kontainer = BasicKontainer()) : Factory<T>, Kontainer by listKontainer {
    override fun create(kontainer: Kontainer): T {
        error("Not supported use instances")
    }
    fun create(kontainer: Kontainer, key: Any): T {
        return listKontainer.get<T>(key)!!.create(kontainer)
    }
}
