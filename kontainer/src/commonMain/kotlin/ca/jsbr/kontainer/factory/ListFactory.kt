@file:Suppress("UNCHECKED_CAST", "unused")

package ca.jsbr.kontainer.factory

import ca.jsbr.kontainer.Kontainer
import ca.jsbr.kontainer.exception.UNALBE_TO_CHANGE_SINGE_LIST
import ca.jsbr.kontainer.exception.UNALBE_TO_RESOLVE


class MutableListFactory<T>(private val items: MutableList<Factory<T>> = ArrayList(), var single: Boolean = false) : Factory<MutableList<T>> {

    private var result: MutableList<T>? = null

    private fun addFactory(factory: Factory<T>) {
        if (single && result != null) UNALBE_TO_CHANGE_SINGE_LIST()
        items.add(factory)
    }

    fun add(vararg factories: Factory<T>): MutableListFactory<T> {
        factories.forEach { addFactory(it) }
        return this
    }

    override fun create(kontainer: Kontainer): MutableList<T> {
        if (single && result != null)
            return result!!
        result = ArrayList(items.map { it.create(kontainer) })
        return result!!
    }
}

inline fun <reified T : Any> Kontainer.singles(vararg factories: Factory<T>) = factories(true, *factories)
inline fun <reified T : Any> Kontainer.providers(vararg factories: Factory<T>) = factories(false, *factories)

inline fun <reified T : Any> Kontainer.factories(single: Boolean, vararg factories: Factory<T>): GenericFactory<List<T>> {
    val factory = (get<T>(List::class)
        ?: set(List::class, GenericFactory<List<T>>())) as GenericFactory<List<T>>
    val listFactory = (factory.get<T>(T::class)
        ?: factory.set(T::class, MutableListFactory<T>(single = single))) as MutableListFactory<T>
    listFactory.add(*factories)
    return factory
}

inline fun <reified T : Any> Kontainer.providers(factories: List<Factory<T>>): GenericFactory<List<T>> {
    val factory = (get<T>(List::class)
        ?: set(List::class, GenericFactory<List<T>>())) as GenericFactory<List<T>>
    factory.set(T::class, MutableListFactory<T>(factories.toMutableList(), false))
    return factory
}


inline fun <reified T : Any> Kontainer.singles(factories: List<Factory<T>>): GenericFactory<List<T>> {
    val factory = (get<T>(List::class)
        ?: set(List::class, GenericFactory<List<T>>())) as GenericFactory<List<T>>
    factory.set(List::class, MutableListFactory<T>(factories.toMutableList(), false))
    return factory
}

/**
 * Create a list instance
 */
inline fun <reified T : Any> Kontainer.instances(): List<T> {
    try {
        return (get<List<T>>(List::class) as? GenericFactory<List<T>>)?.create(this,T::class)
            ?: error("No generic factory associate")
    } catch (err: Throwable) {
        UNALBE_TO_RESOLVE(T::class.simpleName!!, err)
    }
}
