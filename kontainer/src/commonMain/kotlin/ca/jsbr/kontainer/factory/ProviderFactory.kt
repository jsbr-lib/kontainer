@file:Suppress("unused")

package ca.jsbr.kontainer.factory

import ca.jsbr.kontainer.KKey
import ca.jsbr.kontainer.Kontainer

open class ProviderFactory<T>(val action: Kontainer.() -> T) : Factory<T> {
    private var afterInstantiation: (T) -> Unit = {}
    override fun create(kontainer: Kontainer): T {
        val result = kontainer.action()
        afterInstantiation(result)
        return result
    }

    infix fun doAfterInstantiation(action: (T) -> Unit): ProviderFactory<T> {
        afterInstantiation = action
        return this
    }
}


inline fun <reified T : Any> Kontainer.provider(key: Any = T::class, noinline action: Kontainer.() -> T) = set(key, ProviderFactory(action))
fun <T : Any> Kontainer.provider(key: KKey<T>, action: Kontainer.() -> T) = set(key, ProviderFactory(action))
