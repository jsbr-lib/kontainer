@file:Suppress("unused")

package ca.jsbr.kontainer.factory

import ca.jsbr.kontainer.KKey
import ca.jsbr.kontainer.Kontainer

class ValueFactory<out T>(val value: T) : Factory<T> {
    override fun create(kontainer: Kontainer): T = value
}

inline fun <reified T : Any> Kontainer.value(value: T): Factory<T> = set(T::class, ValueFactory(value))
fun <T : Any> Kontainer.value(key: Any, value: T) = set(key, ValueFactory(value))
fun <T : Any> Kontainer.value(key: KKey<T>, value: T) = set(key, ValueFactory(value))
