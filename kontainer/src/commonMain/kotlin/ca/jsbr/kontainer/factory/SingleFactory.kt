@file:Suppress("unused")

package ca.jsbr.kontainer.factory

import ca.jsbr.kontainer.KKey
import ca.jsbr.kontainer.Kontainer

open class SingleFactory<T>(val action: Kontainer.() -> T) : Factory<T> {
    var cache: T? = null
    private var afterInstantiation: (T) -> Unit = {}
    override fun create(kontainer: Kontainer): T {
        if (cache != null)
            return cache!!
        val result = kontainer.action()
        cache = result
        afterInstantiation(result)
        return result
    }

    infix fun doAfterInstantiation(action: (T) -> Unit): SingleFactory<T> {
        afterInstantiation = action
        return this
    }
}


inline fun <reified T : Any> Kontainer.single(key: Any = T::class, noinline action: Kontainer.() -> T) = set(key, SingleFactory(action))
fun <T : Any> Kontainer.single(key: KKey<T>, action: Kontainer.() -> T) = set(key, SingleFactory(action))
