package ca.jsbr.kontainer.factory

import ca.jsbr.kontainer.BasicKontainer
import ca.jsbr.kontainer.ChildKontainer
import ca.jsbr.kontainer.KKey
import ca.jsbr.kontainer.Kontainer

/**
 * Create T element in a not not persistent child container
 * Every time create is called a new container is created and init is called again
 * And every single is reset
 */
class ChildGroupProviderFactory<T : Any>(val init: Kontainer.() -> Factory<T>) : Factory<T> {
    override fun create(kontainer: Kontainer): T {
        val k = BasicKontainer()
        val childKontainer = ChildKontainer(k, kontainer)
        val factory = childKontainer.let(init)
        return factory.create(childKontainer)
    }
}

/**
 * Create a non persistent child container and expose only the last (returned) factory to the parent
 * A new container is created every time the last factory is created.
 * NB: parent factory is available for child container
 */
inline fun <reified T : Any> Kontainer.teen(key: Any = T::class, noinline init: Kontainer.() -> Factory<T>) =
    set(key, ChildGroupProviderFactory(init))

/**
 * Create a non persistent child container and expose only the last (returned) factory to the parent
 * A new container is created every time the las factory is created.
 * NB: parent factory is available for child container
 */
fun <T : Any> Kontainer.teen(key: KKey<T>, init: Kontainer.() -> Factory<T>) = set(key, ChildGroupProviderFactory(init))
