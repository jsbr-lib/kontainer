package ca.jsbr.kontainer.factory

import ca.jsbr.kontainer.BasicKontainer
import ca.jsbr.kontainer.instance
import ca.jsbr.kontainer.kontainer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class SingleFactoryTest {
    @Test
    fun `Should be able to create a single Factory`() {
        SingleFactory { 1 }
    }

    @Test
    fun `Create, should always return the same value`() {
        val kontainer = BasicKontainer()
        var cpt = 0
        val factory = SingleFactory { cpt++ }
        assertEquals(factory.create(kontainer), factory.create(kontainer))
    }

    @Test
    fun `Should be able to be added into the kontainer via single function referenced by KClass`() {
        val kontainer = kontainer {
            single { 1 }
        }
        assertTrue(kontainer.get<Int>(Int::class) is SingleFactory<*>)
        assertEquals(kontainer.instance(), 1)
    }
}