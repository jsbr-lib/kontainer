package ca.jsbr.kontainer.factory

import ca.jsbr.kontainer.BasicKontainer
import ca.jsbr.kontainer.Dummy1
import ca.jsbr.kontainer.instance
import ca.jsbr.kontainer.kontainer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class ChildGroupProviderFactoryTest {
    val dummyKontainer = BasicKontainer()

    @Test
    fun `Should be able to create a single Factory`() {
        ChildGroupProviderFactory { value(1) }
    }

    @Test
    fun `Create, should always return the value of the returned factory`() {
        var cpt = 0
        val factory = ChildGroupProviderFactory { value(1) }
        assertEquals(factory.create(dummyKontainer), 1)
    }

    @Test
    fun `Should be able to be added into the kontainer via teen function referenced by KClass`() {
        val kontainer = kontainer {
            teen{
                value(1)
            }
        }
        assertTrue(kontainer.get<Int>(Int::class) is ChildGroupProviderFactory<*>)
        assertEquals(kontainer.instance(), 1)
    }

    @Test
    fun `should persist single into the created container`() {
        var cpt = 0
        val factory = ChildGroupProviderFactory {
            single { Dummy1() }
            single { instance<Dummy1>() == instance<Dummy1>() }
        }
        assertTrue(factory.create(dummyKontainer))
    }

    @Test
    fun `should not persist single across multiple created container`() {
        var cpt = 0
        val factory = ChildGroupProviderFactory {
            single { Dummy1() }
        }
        assertNotEquals(factory.create(dummyKontainer),factory.create(dummyKontainer))
    }
}