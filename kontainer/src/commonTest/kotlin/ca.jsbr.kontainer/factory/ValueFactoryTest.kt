package ca.jsbr.kontainer.factory

import ca.jsbr.kontainer.BasicKontainer
import ca.jsbr.kontainer.instance
import ca.jsbr.kontainer.kontainer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

class ValueFactoryTest {
    val dummyKontainer = BasicKontainer()

    @Test
    fun `Should be able to create a single Factory`() {
        ValueFactory(1)
    }

    @Test
    fun `Create, should always return the same value`() {
        var cpt = 0
        val factory = ValueFactory(1)
        assertEquals(factory.create(dummyKontainer), factory.create(dummyKontainer))
    }

    @Test
    fun `Should be able to be added into the kontainer via value function referenced by KClass`() {
        val kontainer = kontainer {
            value(1)
        }
        assertTrue(kontainer.get<Int>(Int::class) is ValueFactory<*>)
        assertEquals(kontainer.instance(), 1)
    }
}