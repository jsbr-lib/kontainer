package ca.jsbr.kontainer.factory

import ca.jsbr.kontainer.BasicKontainer
import ca.jsbr.kontainer.instance
import ca.jsbr.kontainer.kontainer
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNotEquals
import kotlin.test.assertTrue

class ProviderFactoryTest {
    val dummyKontainer = BasicKontainer()

    @Test
    fun `Should be able to create a single Factory`() {
        ProviderFactory { 1 }
    }

    @Test
    fun `Create, should return a new value`() {
        var cpt = 0
        val factory = ProviderFactory { cpt++ }
        assertNotEquals(factory.create(dummyKontainer), factory.create(dummyKontainer))
    }

    @Test
    fun `Should be able to be added into the kontainer via provider function referenced by KClass`() {
        val kontainer = kontainer {
            provider { 1 }
        }
        assertTrue(kontainer.get<Int>(Int::class) is ProviderFactory<*>)
        assertEquals(kontainer.instance(), 1)
    }
}