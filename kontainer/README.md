Kontainer
=========

Minimalist dependency injection framework, focusing on flexibility, lightness, personalization and abstraction rather than robustness.

Exemple
--------

```kotlin

class DummyClass(value:string, randomDive:RandomDice){

}

val di = kontainer{ 
  value("key", "value")
  // add DummyClass single factory. Create value only in the fist call and reuse it for other call
  provider {RandomDice(0, 5) } // add static value
  single { DummyClass(instance("key"), instance()) } // add DummyClass single factory. Create value only in the fist call and reuse it for other call
}

val dummyClass:DummyClass = di.instance()
```


